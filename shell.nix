with import <nixpkgs> { };

(mkShell.override { stdenv = stdenvNoCC; }) {
  buildInputs = [
    #nix-linter # currently broken
    nixpkgs-fmt
  ];
}
