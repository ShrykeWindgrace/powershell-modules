{ pkgs }:

let
  posh-git = import ./posh-git/default.nix { inherit pkgs; };
  terminal-icons = import ./Terminal-Icons/default.nix { inherit pkgs; };
  unix-completers = import ./unix-completers/default.nix { inherit pkgs; };
  psscriptanalyzer = import ./PSScriptAnalyzer/default.nix { inherit pkgs; };
in
  {
    regular = posh-git // terminal-icons // unix-completers // psscriptanalyzer;
  }
# //
# pkgs.lib.attrsets.filterAttrs (n: v: pkgs.lib.attrsets.hasAttrByPath [ "pname" ] v) pw

#builtins.concatMap (d: [d.pname]) (builtins.attrValues (pkgs.lib.attrsets.filterAttrs (n: v: pkgs.lib.attrsets.hasAttrByPath [ "pname" ] v) pw))
