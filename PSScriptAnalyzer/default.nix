{ pkgs }:

{
   psscriptanalyzer = import ./../common/fetcher.nix {
    inherit pkgs;
    name = "PSScriptAnalyzer";
    ver = "1.20.0";
    sha = "0rm470n8m4cd0vlpkcc727q2c75970d07ar8wadi49yk2g2l5d2j";
  };
}
