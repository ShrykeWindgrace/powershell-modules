{ pkgs }:

{
  terminal-icons = import ./../common/fetcher.nix {
    inherit pkgs;
    name = "Terminal-Icons";
    ver = "0.11.0";
    sha = "sha256-DVCG+9SLSxLVwAseImOTsyazjD4G2/iCX1IhiOnf5N0=";
  };

}
