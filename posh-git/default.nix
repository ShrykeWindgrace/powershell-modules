{ pkgs }:
{
  posh-git =

    pkgs.stdenv.mkDerivation rec {
      pname = "posh-git";
      version = "1.1.0";
      src = pkgs.fetchFromGitHub {
        owner = "dahlbyk";
        repo = "posh-git";
        rev = "v1.1.0";
        sha256 = "mcqXmDvPR3gZcWVAsdEVvSZyisd87eb74OD62TXD0Hw=";

      };
      #buildInputs = [pkgs.gitFull.git];
      installPhase = let p = "${pname}/${version}"; in
        ''
          echo ${p}
          mkdir -p $out/${p}
          cp -r src/* $out/${p}
        '';
      dontBuild = true;
      dontConfigure = true;
      doInstallCheck = false;
      dontStrip = true;
      dontFixup = true;
    };
}
