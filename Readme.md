# How to instantiate this stuff

```console
$ nix-build -E 'let pkgs = import <nixpkgs>{}; in pkgs.callPackage ./default.nix {inherit pkgs;}'
```

## Usage in config

```nix
{ pkgs, ... }:
let
  pwmods = import
    (builtins.fetchGit {
      url = ''git@gitlab.com:ShrykeWindgrace/powershell-modules.git'';
      ref = "master";
      rev = "3f13253424b1a32cdc085d9adf9c4c837f0969e5"; # or whatever current master revision is right now
    })
    { inherit pkgs; };

  cc = builtins.foldl' (l: r: l + ":${r.outPath}") "$env:PSModulePath += \"" (builtins.attrValues (pkgs.lib.attrsets.filterAttrs (n: v: pkgs.lib.attrsets.hasAttrByPath [ "pname" ] v) pwmods));
in
{
  xdg.configFile."config_file.ps1".text = cc;
}
```

This provides additional paths where powershell looks for modules. Do not forget to dot-source that `config_file.ps1` in the `$PROFILE` of your powershell.
