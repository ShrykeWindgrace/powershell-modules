{ pkgs }:

{
  unix-completers = import ./../common/fetcher.nix {
    inherit pkgs;
    name = "Microsoft.PowerShell.UnixTabCompletion";
    ver = "0.5.0";
    sha = "sha256-SZiDyfNkykeVCUzV3wgvGxVrtRtfqVZrDHKEzSE7GRQ=";
  };
}
